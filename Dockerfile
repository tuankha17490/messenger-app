From node:8

Workdir /app

COPY package.json .
RUN npm install -no-cache
RUN npm install knex -g

COPY . .

RUN npm run build

EXPOSE 8080

CMD ["npm", "start"]