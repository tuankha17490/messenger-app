// eslint-disable-next-line import/no-extraneous-dependencies
import socketIo from 'socket.io';
// eslint-disable-next-line import/no-cycle
import AuthService from '../app/Auth/Services/AuthService';


export default function (server) {
    const io = socketIo(server);
    AuthService.io = io.of('/conversations').on('connection', (socket) => {
        socket.on('join', (data) => {
            // eslint-disable-next-line eqeqeq
            if (data.groupIdGlobal != '') {
                console.log('group id global', data.groupIdGlobal);
                socket.leave(data.groupIdGlobal);
                console.log('someone is joined', data.groupId);
                socket.join(data.groupId);
            } else {
                console.log('someone is joined', data.groupId);
                socket.join(data.groupId);
            }
        });
    });
    AuthService.client = io.of('/news').on('connection', (socket) => {
        socket.on('userConnect', (data) => {
            socket.join(data);
            console.log('connected to my user', data);
        });
    });
}
