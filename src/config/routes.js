import methodOverride from 'method-override';
// import pg from 'pg';
// import session from 'express-session';
// import pgSessionConfig from 'connect-pg-simple';
import routes from '../routes/index';


export default function (app) {
  app.use(methodOverride('X-HTTP-Method-Override'));

<<<<<<< HEAD
//   const pgSession = pgSessionConfig(session);
//   const option = {
//   server: 'localhost',
//   port: '5432',
//   database: 'messageapp',
//   user: 'kha',
//   password: '628638',

// };
  // const pgPool = new pg.Pool(option);

  // app.use(session({
  //     store: new pgSession({
  //     pool: pgPool, // Connection pool
  //     tableName: 'user_sessions', // Use another table-name than the default "session" one
  //   }),
  //   key: 'messageapp',
  //   secret: 'messageapp',
  //   saveUninitialized: true,
  //   resave: false,
  //   cookie: { maxAge: 30 * 24 * 60 * 60 * 1000 }, // 30 days
  // }));
=======
  const pgSession = pgSessionConfig(session);
  const option = {
  server: 'localhost',
  port: '5432',
  database: 'messageapp',
  user: 'postgres',
  password: '628638',

};
  const pgPool = new pg.Pool(option);

  app.use(session({
      store: new pgSession({
      pool: pgPool, // Connection pool
      tableName: 'user_sessions', // Use another table-name than the default "session" one
    }),
    key: 'messageapp',
    secret: 'messageapp',
    saveUninitialized: true,
    resave: false,
    cookie: { maxAge: 30 * 24 * 60 * 60 * 1000 }, // 30 days
  }));
>>>>>>> 5fe5e79e6e3ee87692cd988b77003426524bed5a
  app.use(
    methodOverride((req) => {
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        const method = req.body._method;
        delete req.body._method;

        return method;
      }

      return undefined;
    }),
  );

  app.use(async (req, res, next) => {
    res.redirectBack = () => {
      const backURL = req.header('Referer') || '/';
      return res.redirect(backURL);
    };

    res.locals.firebaseApiKey = process.env.FIREBASE_API_KEY;
    res.locals.firebaseProjectId = process.env.FIREBASE_PROJECT_ID;
    res.locals.firebaseSenderId = process.env.FIREBASE_SENDER_ID;
    res.locals.firebaseAppId = process.env.FIREBASE_APP_ID;

    next();
  });

  app.use(routes);
}
