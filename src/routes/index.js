import express from 'express';
// eslint-disable-next-line import/no-cycle
import authRouter from '../app/Auth/Routes/routes';
import { verifyAuthentication } from '../app/Auth/Middleware/AuthMiddleware';
// eslint-disable-next-line import/no-cycle
import Controller from '../app/Auth/Controllers/AuthController';

const router = express.Router();
const controller = new Controller();

router.use(authRouter);

router.get('/', (req, res) => res.redirect('/conversations'));

router.get('/conversations', verifyAuthentication, controller.friendList);

// router.get('/loadChat', controller.loadChat);

export default router;
