import knex from '../../database/connection';

class BaseRepository {
  constructor() {
    this.tableName = this.getTableName();
    this.tableFriend = this.getFriendName();
  }

  cloneQuery() {
    return knex(this.tableName).clone();
  }

  queryFriend() {
    return knex(this.getFriendName()).clone();
  }

  list(columns = ['*']) {
    return this.cloneQuery().select(columns);
  }

  listBy(clauses = {}, columns = ['*']) {
    return this.queryFriend().where(clauses).select(columns);
  }

  listByJoin(clauses = {}, columns = ['*']) {
    return this.queryFriend().innerJoin('users', 'friends.userId', 'users.id').where(clauses).select(columns);
  }

  count() {
    return this.cloneQuery().count();
  }

  countBy(clauses = {}) {
    return this.cloneQuery().where(clauses).count();
  }

  getBy(clauses = {}, columns = ['*']) {
    return this.cloneQuery().where(clauses).select(columns).first();
  }

  create(attributes, returning = ['*']) {
    return this.cloneQuery().insert(attributes).returning(returning);
  }

  update(clauses, attributes, returning = ['*']) {
    return this.queryFriend().where(clauses).update(attributes).returning(returning);
  }

  delete(clauses) {
    return this.queryFriend().where(clauses).delete();
  }

  first(clauses = {}, columns = ['*']) {
    return this.cloneQuery().where(clauses).first(columns);
  }

  createFriend(attributes) {
    return this.queryFriend().insert(attributes);
  }

  firstFriend(clauses = {}, columns = ['*']) {
    return this.queryFriend().where(clauses).first(columns);
  }
}

export default BaseRepository;
