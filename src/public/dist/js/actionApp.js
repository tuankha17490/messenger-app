

function editProfile()
{
    $('.update').click(function(event) {
        const file = $('#avatar').get(0).files[0];
        console.log(file);        
        var formData = new FormData();
        formData.append('email',$('#email').val());
        formData.append('fullname',$('#fullname').val());
        formData.append('city',$('#city').val());
        formData.append('phone',$('#phone').val());
        formData.append('avatar',file);
        
        var user = firebase.auth().currentUser;

        user.updateEmail($('#email').val()).then(function() {
        // Update successful.
        }).catch(function(error) {
        // An error happened.
        });

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, save it!'
          }).then((result) => {
            if (result.value) {
                $.ajax({
                    url:`/update-profile`,
                    dataType:'JSON',
                    type: 'PUT',
                    data : formData,
                    processData: false,
                    contentType: false,
                }).then(function(res){
                    if (res.type == 'success') {
                        Swal.fire(
                            'Saved!',
                            'Your edit has been saved.',
                            'success'
                        ).then(function() {
                            window.location.href = '/';
                        }).catch(error => {
                            console.log('Loi update',error);
                            
                        });
                    }
                });
               
            }
          })

      
    })
}

function changeNumber(getphone) {
    if (getphone.substring(0,1)== '0') {
        const change = '+84';
        var newphone = change + getphone.substring(1,getphone.length);
    
    }else{
        var newphone = getphone;
    };
    return newphone;
}


function addFriend() {
    $('#addFriend').click(function(){
        const userInfo =  $('#userInfo').val();
        console.log(userInfo);
        
        const number = userInfo.search("@");
        const message = $('#message').val();
        if (number != -1) {
            $.ajax({
                url: `/add-friend-email`,
                dataType: 'JSON',
                type: 'POST',
                data:{
                    email:userInfo,
                    message
                }
            }).then(res => {
                if (res.type == 'success') {
                    Swal.fire({
                        type: 'success',
                        title: 'Success',
                        showCancelButton: false,
                        timer: '1500'
                    }).then(function () {
                        window.location.href = '/';
                    })
                }
                if (res.type == 'failed') {
                    Swal.fire({
                        type: 'warning',
                        timer: '1500',
                        title: 'Oopss...',
                        text: 'Do not have this account. Please try again'
                    })
                }
                if (res.type == 'exist') {
                    Swal.fire({
                        type: 'warning',
                        timer: '1500',
                        title: 'Oopss...',
                        text: 'This account was your friend '
                    })
                }
                if (res.type == 'send') {
                    Swal.fire({
                        type: 'warning',
                        timer: '1500',
                        title: 'Oopss...',
                        text: 'You have send request to this account'
                    })
                }
                
            }).catch(error => {
                console.log(error);
                
            })
        }
       
    });
}


function addFriendPhone() {
    $('#addFriend').click(function () {
        const userInfo =  $('#userInfo').val();
        const number = userInfo.search("@");
        const message = $('#message').val();
        
        if(number == -1){
            const phone =changeNumber(userInfo);
            if (phone.substring(0,1) == "+") {
                console.log('abcxyz');
                const check = /^\d+$/;            
                if (check.test(phone.substring(1,phone.length)) === true) {
                    $.ajax({
                        url: `/add-friend-phone`,
                        dataType: 'JSON',
                        type: 'POST',
                        data: {
                            phone:phone,
                            message
                        }
                    }).then(res => {
                        if (res.type == 'success') {
                            Swal.fire({
                                type: 'success',
                                title: 'Success',
                                showCancelButton: false,
                                timer: '1500'
                            }).then(function () {
                                window.location.href = '/';
                            })
                        }
    
                        if (res.type == 'failed') {
                            Swal.fire({
                                type: 'warning',
                                timer: '1500',
                                title: 'Oopss...',
                                text: 'Do not have this account. Please try again'
                            })
                        }
                        if (res.type == 'exist') {
                            Swal.fire({
                                type: 'warning',
                                timer: '1500',
                                title: 'Oopss...',
                                text: 'This account was your friend '
                            })
                        }
                        if (res.type == 'send') {
                            Swal.fire({
                                type: 'warning',
                                timer: '1500',
                                title: 'Oopss...',
                                text: 'You have send request to this account'
                            })
                        }
                    })
                }else {
                    Swal.fire({
                        type: 'error',
                        title: 'Error',
                        text: 'Email or Phone number is invalid. Please try again',
                        showCancelButton: false,
                        timer: '3000',
                        position: 'top'
                    })
                }
            }
        }
        
    })
}


function accept() {
    $('.accept').click(function () {
        const userId = $(this).closest('li').data('id');
        console.log('deny',userId);
        $.ajax({
            url: `/accept-friend`,
            dataType: 'JSON',
            type: 'PUT',
            data: {
                state:1,
                userId
            }
        }).then(res => {
            if (res.type== 'success') {
                Swal.fire({
                    type: 'success',
                    title: 'Congratulation',
                    showCancelButton: false,
                    timer: 2000
                }).then(function () {
                    window.location.href= '/';
                })
            }
        })
    })
}

function deny() {
    $('.deny').click(function () {
        const userId = $(this).closest('li').data('id');
        console.log('deny',userId);
        $.ajax({
            url: `/deny-friend`,
            dataType: 'JSON',
            type: 'DELETE',
            data: {userId}
        }).then(res => {
            if (res.type== 'success') {
                Swal.fire({
                    type: 'success',
                    title: 'Congratulation',
                    showCancelButton: false,
                    timer: 2000
                }).then(function () {
                    window.location.href= '/';
                })
            }
        })
    })
}


function profile() {
    $('.profile').click(function () {
        const profileId = $(this).closest('li').data('id');
        console.log(profileId);
        $.ajax({
            url:`/`,
            dataType: 'JSON',
            type: 'GET',
            data: {userId}
        }).catch((error) => {
            console.log(error);
        })
        
    })
}


function loadChat() {
    $.ajax({
        url: `/loadChat`,
        type: 'GET',
        dataType: 'JSON',
    }).then(res => {
        if (res.type == 'success') {
            const data = res.data;
            console.log('da nhan duoc du lieu roi nghe =====================',data);
            const messageOthers = data.messageOther;
            const messageUsers = data.messageUser[0].content[0];
            messageOthers.forEach(messageOther => {
                for (let t = 0; t < messageOthers.length; t++) {  
                    if (data.messageUser[0].memberId == messageOther.memberId) {
                        messageUsers.forEach(messageUser => {
                            
                        });
                        $('.messages').append( "<div class='outgoing-message' ><div class='message-content' >" + data.messageUser[0].content[0] + "</div><div class='message-action' ></div></div><br/>");
                    }
                    else{
                        $('.messages').append( "<div class='message-item' ><div class='message-content' >" + messageOther.content[i] + "</div><div class='message-action' ></div></div><br/>"); 
                    }
                }
                console.log('memberID====',messageOther.memberId);
                
            });
        }
    })
}


var a = [];
function findFriend() {
    $('input[name = "find"]').val('');
    // $('#myInput').click(function() {
    //     $("#myDropdown").toggleClass("show");
    //     $(".dropdown-content").css("display","block");

    // })
    $('.findFriend').keyup(function() {
        const temp = $('.findFriend').val();
        if(temp != '')
        {
            $('.showFriend').html("<div></div>");
            $.ajax({
                url: `/find-friend`,
                type: 'POST',
                dataType: 'JSON',
                data: {
                    string: temp,
                }
            }).then(res => {
                const friends = res.query;
                console.log('query', friends);
                if(friends.length == 1)
                {
                    $(".dropdown-content").css({"display":"block","height": "65px"});
                }
                if(friends.length == 2) {
                    $(".dropdown-content").css({"display":"block","height": "130px"});
                }
                if(friends.length > 2) {
                    $(".dropdown-content").css({"display":"block","height": "260px"});
                }
                friends.forEach(friend => {
                    if(friend.avatar == undefined || friend.avatar == '') {
                        $('.showFriend').append("<div><a data-avatar = '"+ friend.avatar +"' data-id = '"+ friend.userId +"' class = 'selectFriend'><figure class = 'avatar'><img class = 'rounded-circle' src = 'dist/images/avatar/defaultAvatar.png'></figure>"+ friend.fullName +"</a></div>");
                    }
                    else {
                        $('.showFriend').append("<div><a data-avatar = '"+ friend.avatar +"' data-id = '"+ friend.userId +"' class = 'selectFriend'><figure class = 'avatar'><img class = 'rounded-circle' src = '" + friend.avatar + "'></figure>"+ friend.fullName +"</a></div>");
                    }
                });
                $('.selectFriend').click(function() {
                    const friendId = $(this).closest('a').data('id');
                    const friendAvatar = $(this).closest('a').data('avatar');
                    console.log('friend id',friendId);
                    var t = 0;
                    for (let i = 0; i < a.length; i++) {
                        if(a[i] == friendId) {
                            t = 1;
                        }
                    }
                    if(t == 0) {
                        $('.showFriend').html('<div></div>');
                        $(".dropdown-content").css("display","none");
                        if(friendAvatar == 'undefined' || friendAvatar == '') {                         
                            $('.avatar-group').append("<figure id = 'userAdded' data-id = '"+ friendId +"' class= 'avatar'><img class = 'rounded-circle' src = 'dist/images/avatar/defaultAvatar.png'></figure>")
                        } else {
                            $('.avatar-group').append("<figure id = 'userAdded' data-id = '"+ friendId +"' class= 'avatar'><img class = 'rounded-circle' src = '"+ friendAvatar +"'></figure>")
                        }
                        $('input[name = "find"]').val('');
                        a.push(friendId);
                    }
                    if(t == 1) {
                        $('.showFriend').html("<div></div>");
                        $('input[name = "find"]').val('');
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: 'This Member Is Added Into This Group',
                            showCancelButton: false,
                            timer: '3000',
                            position: 'top'
                        });
                    }
                    $('#userAdded').click(function() {
                        const userAddedId = $(this).closest('figure').data('id');
                        const location = a.indexOf(userAddedId);               
                        a.splice(location,1);
                        $(this).closest('figure').html('<div></div>');
                    })
                });
                
            })
        }
        else
        {
            $(".dropdown-content").css("display","none");
            $('.showFriend').html("<div></div>");
        }
    });
   
}

function createGroup() {
    $('.createGroup').click(function () {
        const groupMember = a;
        console.log('group member',groupMember);
        const groupName = $('#groupName').val();
        const groupDesrip = $('#description').val();
        $.ajax({
            url: `/create-group`,
            dataType: 'JSON',
            type: 'POST',
            data: {
                groupMember: JSON.stringify(groupMember),
                groupName,groupDesrip, 
            }
        }).then(res => {
            if (res.type == 'success') {
                Swal.fire({
                    type: 'success',
                    title: 'Congratulation',
                    showCancelButton: false,
                    timer: 2000
                }).then(function () {
                    window.location.href= '/';
                }) 
            }
        })
        
    })
}


$(document).ready(function(){
    createGroup();
    editProfile();
    addFriend();
    addFriendPhone();
    accept();
    deny();
    findFriend();
});






