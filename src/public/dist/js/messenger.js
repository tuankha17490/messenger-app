var groupIdGlobal= '';
var test ='';
function changeGroup(socket) {
    var groupId;
    var head;
    $('.list-group-item').click(function () {
        test = 2;
        $('.chat-body .messages').empty();
        if(groupIdGlobal) {
            $('.'+ groupIdGlobal).removeClass('open-chat');
            $('.'+ groupIdGlobal +' .users-list-action').removeClass('action-toggle');
            $('.'+ groupIdGlobal +' .users-list-action').empty();
        }
        groupId = $(this).closest('li').data('id');
        head = $('.list-group-item').closest('li').data('head')
        const groupName = $(this).closest('li').data('name');
        socket.emit('join', {groupId, groupIdGlobal});
        groupIdGlobal = groupId;
        $('.'+ groupId +' .avatar').removeClass('avatar-state-success');
        $('.'+ groupId).addClass('open-chat');
        $('.open-chat .users-list-action').empty();
        $('.open-chat .users-list-action').addClass('action-toggle');
        $('.open-chat .action-toggle').append("<div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item active' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div>")
        $.ajax({
            url: `/messages/${groupId}`,
            type: 'POST',
            dataType: 'JSON',
            data: {groupId},
        }).then(res => {
            const messageItems = res.messages;
            const id = res.userId;
            var dem = 1;
            var temp;
            messageItems.forEach(messageItem => {
                if(temp == messageItem.memberId.id) {
                    dem++;
                } else {
                    dem = 1;
                }
                temp = messageItem.memberId.id;
                if(messageItem.memberId.id == id[0].id) {
                    $('.messages').append( "<div class='message-item outgoing-message ' ><div class='message-content' >" + messageItem.content + "</div><div class='message-action' ></div></div><br/>");
                }
                else {
                    if(dem == 1) {
                        $('.messages').append("<div class = 'messageUser'>"+ messageItem.memberId.fullName +"</div>");
                        $('.messages').append( "<div class='message-item' ><div class = 'message-component'><figure class= 'avatar'><img class = 'rounded-circle' src = 'dist/images/avatar/defaultAvatar.png'></figure><div class='message-content' >" + messageItem.content + "</div></div><div class='message-action' ></div></div><br/>");
                    } else {
                        $('.messages').append( "<div class='message-item' style = 'margin-left:2.5rem; padding-left: 20px' ><div class = 'message-component'><div class='message-content' >" + messageItem.content + "</div></div><div class='message-action' ></div></div><br/>");
                    }
                    
                }
            });
        })
    })
    $('#chat-submit').click(function(){
        if(test == 2) {
            const message = $('#chat-input').val();
            $.ajax({
                url: `/send-message`,
                type: 'POST',
                dataType: 'JSON',
                data: {message, groupId},
            }).then(res => {
                if(res.type == 'success') {
                    if(groupId != head) {
                        $('.'+ head).before($('.'+ groupId));
                        $('.list-group-item').attr('data-head',() => groupId);
                        $('.'+ groupId +' .users-list-body p').html(message);
                    }
                }
            })
        }
    });
}

function start(groupId, socket) {
    socket.emit('join', {groupId, groupIdGlobal});
    groupIdGlobal = groupId;
    $('#chat-submit').click(function(){
        if(test != 2) {
            const message = $('#chat-input').val();
            $.ajax({
                url: `/send-message`,
                type: 'POST',
                dataType: 'JSON',
                data: {message, groupId},
            }).then(res => {
                $('.'+ groupId +' .users-list-body p').html(message);
            });
        }
    });
    $('.'+ groupId +' .avatar').removeClass('avatar-state-success');
    $('.'+ groupId).addClass('open-chat');
    $('.open-chat .users-list-action').empty();
    $('.open-chat .users-list-action').addClass('action-toggle');
    $('.open-chat .action-toggle').append("<div class='dropdown'><a data-toggle='dropdown' href='#'><i class='ti-more'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Open</a><a class='dropdown-item active' href='#' data-navigation-target='contact-information'>Profile</a><a class='dropdown-item' href='#'>Add to archive</a><a class='dropdown-item' href='#'>Delete</a></div></div>")
    $.ajax({
        url: `/messages/${groupId}`,
        type: 'POST',
        dataType: 'JSON',
        data: {groupId},
    }).then(res => {
        const messageItems = res.messages;
        const id = res.userId;
        var dem = 1;
        var temp;
        messageItems.forEach(messageItem => {
            if(temp == messageItem.memberId.id) {
                dem++;
            } else {
                dem = 1;
            }
            temp = messageItem.memberId.id;
            if(messageItem.memberId.id == id[0].id) {
                $('.messages').append( "<div class='message-item outgoing-message ' ><div class='message-content' >" + messageItem.content + "</div><div class='message-action' ></div></div><br/>");
            }
            else {
                if(dem == 1) {
                    $('.messages').append("<div class = 'messageUser'>"+ messageItem.memberId.fullName +"</div>");
                    $('.messages').append( "<div class='message-item' ><div class = 'message-component'><figure class= 'avatar'><img class = 'rounded-circle' src = 'dist/images/avatar/defaultAvatar.png'></figure><div class='message-content' >" + messageItem.content + "</div></div><div class='message-action' ></div></div><br/>");
                } else {
                    $('.messages').append( "<div class='message-item' style = 'margin-left:2.5rem; padding-left: 20px' ><div class = 'message-component'><div class='message-content' >" + messageItem.content + "</div></div><div class='message-action' ></div></div><br/>");
                }
                
            }
        });
    })
}


var t;
function excuteSocket(socket) {
    $('.list-group-item').click(function (){
        const id = $(this).closest('li').data('userid');
        t = id;
    });
    if(t == '' || t == undefined) {
        t = $('.list-group-item').closest('li').data('userid');
    }
    var temp;
    var dem = 1;
    socket.on('message', (data) => {
        console.log('da nhan duoc du lieu', data);
        if(data.userId != t) {
            $.ajax({
                url: `/get-user-information`,
                type: 'POST',
                dataType: 'JSON',
                data: { userId: data.userId },
            }).then(res => {
                if(temp == data.userId) {
                    dem++;
                } else {
                    dem = 1;
                }
                temp = data.userId;
                if(dem == 1) {
                    const item = res.infor;
                    $('.messages').append("<div class = 'messageUser'>"+ item[0].fullName +"</div>");
                    if(item[0].avatar == '' || item[0].avatar == undefined) {
                        $('.messages').append( "<div class='message-item' ><div class = 'message-component'><figure class= 'avatar'><img class = 'rounded-circle' src = 'dist/images/avatar/defaultAvatar.png'></figure><div class='message-content' >" + data.message + "</div></div><div class='message-action' ></div></div><br/>");
                    } else {
                        $('.messages').append( "<div class='message-item' ><div class = 'message-component'><figure class= 'avatar'><img class = 'rounded-circle' src = '"+ item[0].avatar +"'></figure><div class='message-content' >" + data.message + "</div></div><div class='message-action' ></div></div><br/>");
                    }
                } else {
                    $('.messages').append( "<div class='message-item' style = 'margin-left:2.5rem; padding-left: 20px' ><div class = 'message-component'><div class='message-content' >" + data.message + "</div></div><div class='message-action' ></div></div><br/>");
                }
                
               
            })
            
        }

    });
}

// function chat(){
//     const today = new Date();   
//     const time = today.getHours() + ":" + today.getMinutes();
//     if (today.getHours() > 0 && today.getHours()<12) {
//        var a = 'AM ';
//     } else {
//         var a = 'PM ';
//     }
//     const session = a;
//     const socket = io.connect('http://localhost:3000');
//     socket.on('connect', function(data) {
//        socket.emit('join', 'Hello World from client');
//     });
//     $('#chat-submit').click(function(){
//         const message = $('#chat-input').val();
//         socket.emit('message',message);
//     });
//     socket.on('broadcast',function(message){
//         console.log('broad',message);
//         $('.messages').append( "<div class='message-item' ><div class='message-content' >" + message + "</div><div class='message-action' >" + session + time + "</div></div><br/>");
        
//     });

// }

function socketUser(socket) {
    const userId = $('.list-group-item').closest('li').data('myid');
    console.log('userid---->', userId);
    socket.emit('userConnect', userId);
    socket.on('newMessage', (data) => {
        console.log('data ------>', data);
        const headId = $('.list-group-item').closest('li').data('head');
        $('.'+ headId).before($('.'+ data.groupId));
        $('.list-group-item').attr('data-head',() => data.groupId);
        $('.'+ data.groupId +' .users-list-body p').html(data.message);
        if($('.'+ data.groupId).hasClass('.open-chat') == false) {
            $('.'+ data.groupId +' .avatar').addClass('avatar-state-success');
            $('.'+ data.groupId +' .users-list-action').html("<div class = 'new-message-count'>1</div>");
        }
    });
}


$(document).ready(function(){
    const socket = io.connect('/conversations');
    const news = io.connect('/news');
    const headId = $('.list-group-item').closest('li').data('head');
    if(test == '') {
        start(headId, socket);
    }
    changeGroup(socket);
    excuteSocket(socket);
    socketUser(news);
})