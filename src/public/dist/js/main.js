
$(document).ready(function() {

    $('#register').submit(function(event) {
        event.preventDefault();
      
        const email= $('input[name="email"]').val();
        const password= $('input[name="password"]').val();
        
        firebase.auth().createUserWithEmailAndPassword(email,password).then(function() {

            $.ajax({
                url: `/register-email`,
                dataType: 'JSON',
                type: 'POST',
                data: {
                    fullName: $('input[name="fullName"]').val(),
                    email: $('input[name="email"]').val(),
                    password: $('input[name="password"]').val(),
                   
                }
            }).done(res => {
                if(res.type == 'success') {
                   Swal.fire(
                    'Sign Up Success!',
                    'Please Check Your Email To Verify!',
                    'success'
                  ).then(function () {
                      window.location.href = '/login';
                  });
                }
            });

            const user = firebase.auth().currentUser;
            user.sendEmailVerification().then(function() {
                // Email sent.
                console.log("email has verified");
                
              }).catch(function(error) {
                  console.log(error);
                  
                // An error happened.
              });
            }).catch(function(error) {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: error.message,
                    position: 'top',
                    timer : '4000',
                    showConfirmButton: false,
                }).then(function () {
                    window.location.href = '/register-email';
                })
                // An error happened.
                console.log('error',error);
                
            });
  
    });

  
    $('#login-form').submit(function(event) {
        event.preventDefault();
        const email= $('input[name="email"]').val();
        const password= $('input[name="password"]').val();
        firebase.auth().signInWithEmailAndPassword(email,password).then(cred => {
            console.log(cred.user);
            if (cred.user.emailVerified == false) {
                Swal.fire({
                    type: 'error',
                    title: 'Oops.....',
                    text: 'Please Verified Email If You Want Logn In'
                }).then(function(){
                    window.location.href = '/login';
                });
            }
            if(cred.user.emailVerified == true){
                $.ajax({
                    url:`/login`,
                    dataType: 'JSON',
                    type:'POST',
                    data: {email,password}
                }).then((res)=>{
                    if (res.type =='success'){
                        window.location.href = '/';
                    }
                }).catch(error =>{
                    console.log(error);
                    
                })
            }
            
        }).catch(error=> {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: error.message,
                position: 'top',
                timer : '3000',
                showConfirmButton: false,
            }).then(function () {
                window.location.href = '/login';
            })
            
        })
        
    });


    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    $('#register-phone').submit( function (event){
        event.preventDefault();
        const appVerifier = window.recaptchaVerifier;
        const fullName = $('input[name="fullName"]').val();
        const getphone = $('input[name="phoneNumber"]').val();    
        if (getphone.substring(0,1)== '0') {
            const change = '+84';
            var newphone = change + getphone.substring(1,getphone.length);
        
        }else{
            var newphone = getphone;
        };

        const phoneNumber = newphone;  

        firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
        .then(function (confirmationResult) {
            window.confirmationResult = confirmationResult;
            $('#register-info').css('display','none');
            $('#register-confirm').css('display','block');
            $('#register-confirm').submit(function(event) {
                event.preventDefault();
                const testVerificationCode = $('input[name="code"]').val();
                if (!testVerificationCode) {
                    alert('Do not have anything on the input.Please input again');
                };
                return confirmationResult.confirm(testVerificationCode).then(function (cred) {
                    firebase.auth().currentUser.getIdToken(true).then(function(idToken) {
                    console.log("idToken",idToken);
                    $.ajax({
                        url:`/register-phone`,
                        dataType: 'JSON',
                        type: 'POST',
                        data: {
                            idToken: idToken,
                            fullName:fullName,
                            phoneNumber:phoneNumber
                        }
                    }).then((res)=>{
                        if (res.type == 'success') {
                            Swal.fire({
                                type: 'success',
                                title: 'Congratulation'
                            }).then(function(){
                                window.location.href = '/login-phone-number';
                            });
                           
                        }
                    });

                      }).catch(function(error) {
                       console.log('error idtoken',error);
                        
                      });
                    console.log(cred);
                    console.log('send data to server successfully');
                    
                    
                });
               
            });
            
        }).catch(function (error) {
            console.log('error',error)
        });

    });

    $('#login-phone-number').submit(function (event) {
        event.preventDefault();
        const getphone = $('input[name="phoneNumber"]').val();
        const appVerifier = window.recaptchaVerifier;
        if (getphone.substring(0,1)== '0') {
            const change = '+84';
            var newphone = change + getphone.substring(1,getphone.length);
        
        }else {
            var newphone = getphone;
        };

        const phoneNumber = newphone;
        console.log(phoneNumber);
        
        
        firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
        .then(function (confirmationResult) {
            window.confirmationResult = confirmationResult;
            
            $('#login-info').css('display','none');
            $('#login-confirm').css('display','block');
            $('#login-confirm').submit(function(event) {
                event.preventDefault();
                const testVerificationCode = $('input[name="code"]').val();
                if (!testVerificationCode) {
                    alert('Do not have anything on the input.Please input again');
                };
                return confirmationResult.confirm(testVerificationCode).then((cred)=> {
                    $.ajax({
                        url:`/login-phone-number`,
                        dataType:'JSON',
                        type:'POST',
                        data: {
                            phoneNumber:phoneNumber
                        }
                    }).then((res)=> {
                        if (res.type == 'success') {
                            window.location.href = '/';
                        }
                    })
                    
                }).catch((error)=> {
                    console.log(error);
                    alert(error.message);
                    
                })
            })
        });
    });



});



