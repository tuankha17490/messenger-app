// eslint-disable-next-line no-unused-vars
import { create } from 'domain';
import BaseController from '../../../infrastructure/Controllers/BaseController';
// eslint-disable-next-line import/no-cycle
import Service from '../Services/AuthService';


class AuthController extends BaseController {
  constructor() {
    super();
    this.service = Service.getService();
  }

  load(req, res) {
    return Service.getService().load(req, res);
  }

  getInfor(req, res) {
    return Service.getService().getInfor(req, res);
  }

  logout(req, res) {
    delete req.session.user;
    return res.redirect('/login');
  }

  registerEmail(req, res) {
    return Service.getService().registerEmail(req, res);
  }

  createGroup(req, res) {
    return Service.getService().createGroup(req, res);
  }

  registerPhone(req, res) {
   return Service.getService().registerPhone(req, res);
  }


  loginPhone(req, res) {
    return Service.getService().loginPhone(req, res);
  }

  loginEmail(req, res) {
    return Service.getService().loginEmail(req, res);
  }

  updateProfile(req, res) {
    return Service.getService().updateProfile(req, res);
  }

  addFriendEmail(req, res) {
    return Service.getService().addFriendEmail(req, res);
  }

  addFriendPhone(req, res) {
    return Service.getService().addFriendPhone(req, res);
  }

  friendList(req, res) {
    return Service.getService().friendList(req, res);
  }

  acceptFriend(req, res) {
    return Service.getService().acceptFriend(req, res);
  }

  denyFriend(req, res) {
    return Service.getService().denyFriend(req, res);
  }

  findFriend(req, res) {
    return Service.getService().findFriend(req, res);
  }

  loadChat(req, res) {
    return Service.getService().loadChat(req, res);
  }

  messageLoad(req, res) {
    return Service.getService().messageLoad(req, res);
  }

  sendMessage(req, res) {
    return Service.getService().sendMessage(req, res);
  }
}

export default AuthController;
