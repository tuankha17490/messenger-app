const admin = require('firebase-admin');
const serviceAccount = require('../../../../serviceAccount.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://messengerapp-e105d.firebaseio.com',
  });

const verifyAuthentication = (req, res, next) => {
    if (req.session.user) {
        return next();
    }

    return res.redirect('/login');
};

const verifyNotAuthtication = (req, res, next) => {
    if (!req.session.user) {
        return next();
    }

    return res.redirect('/');
};


const verifyToken = (req, res, next) => {
    const { idToken } = req.body;
    console.log(idToken);

    admin.auth().verifyIdToken(idToken)
    .then((decodedToken) => {
      const { uid } = decodedToken;
      console.log(uid);
    }).catch((error) => {
        console.log(error);

      // Handle error
    });
    next();
};


export { verifyToken, verifyAuthentication, verifyNotAuthtication };
