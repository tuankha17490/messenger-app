/* eslint-disable import/no-cycle */
// eslint-disable-next-line import/no-extraneous-dependencies
import Moment from 'moment';
import Repository from '../Repositories/AuthRepository';
import { s3Upload } from '../../../utils/s3Upload';
import { resizeImg } from '../../../utils/helper';
import Model from '../../../database/models/index';

class AuthService {
  static service;

  static io;

  static client;

  constructor() {
    this.repository = Repository.getRepository();
  }

  static getService() {
    if (!this.service) {
      this.service = new this();
    }
    return this.service;
  }

  async registerEmail(req, res) {
    try {
      const data = req.body;
      const clauses = { email: data.email };
      const attributes = {
        fullName: data.fullName,
        email: data.email,
        password: data.password,
      };
      const returning = ['*'];
      const column = ['*'];
      const check = await Repository.getRepository().first(clauses, column);
      if (check) {
        // eslint-disable-next-line no-console
        console.log('email has be used another user', check);
        return res.redirect('/register-email');
      }
        await Repository.getRepository().create(attributes, returning);
        const id = await Repository.getRepository().first(attributes, ['id']);
        await Model.User.create({
          id,
          fullName: data.fullName,
        });
          return res.json({ type: 'success' });
  } catch (error) {
      // eslint-disable-next-line no-console
      console.log('loi catch', error);
      }
  }

  async registerPhone(req, res) {
    try {
      const data = req.body;
      const clauses = { phoneNumber: data.phoneNumber };
      const attributes = {
        fullName: data.fullName,
        phoneNumber: data.phoneNumber,
      };
      const returning = ['*'];
      const column = ['*'];
      const check = await Repository.getRepository().first(clauses, column);

      if (check) {
          return res.redirect('/register-phone');
        }

      await Repository.getRepository().create(attributes, returning);
      const id = await Repository.getRepository().first(attributes, ['id']);
      await Model.User.create({
        id,
        fullName: data.fullName,
      });
      return res.json({ type: 'success' });
  } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
    }
  }

  async loginPhone(req, res) {
    const { phoneNumber } = req.body;
    const clauses = { phoneNumber };
    const column = ['*'];
    const check = await Repository.getRepository().first(clauses, column);
    // eslint-disable-next-line no-console
    console.log(check);
    if (!check) {
        return res.redirect('/login-phone-number');
    }

    req.session.user = check;
    return res.json({ type: 'success' });
  }

  async loginEmail(req, res) {
    const data = req.body;
    const clauses = { email: data.email };
    const column = ['*'];
    const check = await Repository.getRepository().first(clauses, column);
    if (!check) {
        return res.redirect('/login');
    }

    // eslint-disable-next-line eqeqeq
    if (check.password == data.password) {
      req.session.user = check;
      return res.json({ type: 'success' });
    }
  }

  async updateProfile(req, res) {
    const data = req.body;
    // eslint-disable-next-line no-console
    console.log(data);
    const image = req.file.name;
    // eslint-disable-next-line no-console
    console.log(image);

    const clauses = { fullName: req.session.user.fullName, email: req.session.user.email };

    // eslint-disable-next-line no-undef
    const avatarResize = await resizeImg(avatar);
    // eslint-disable-next-line no-console
    console.log('avatar resize', avatarResize);
    // eslint-disable-next-line no-undef
    const url = await s3Upload(avatarResize, avatar);
    // eslint-disable-next-line no-console
    console.log('url', url);

    const attributes = {
      fullName: data.fullName,
      email: data.email,
      avatar: data.avatar,
      city: data.city,
      phoneNumber: data.phone,
    };
    const returning = ['*'];
    await Repository.getRepository().update(clauses, attributes, returning);
    req.session.user = data;
    await Model.User.update({ attributes },
      {
        $set: {
          fullName: data.fullName,
          avatar: data.avatar,
        },
      });
    // req.session.user.avatar = url;
    return res.json({ type: 'success' });
  }


  async addFriendEmail(req, res) {
    const data = req.body;
    const { user } = req.session;
    const clauses = { fullName: user.fullName, email: user.email, phoneNumber: user.phoneNumber };
    const column = ['id'];
    const emailCheck = { email: data.email };
    const userId = await Repository.getRepository().first(clauses, column);
    const friendId = await Repository.getRepository().first(emailCheck, column);
    const action = await Repository.getRepository().listBy({ userId: friendId.id, receivedId: req.session.user.id }, ['*']);

    // eslint-disable-next-line eqeqeq
    if (action[0] == undefined) {
      if (friendId) {
        const attributes = {
          userId: userId.id,
          friendId: friendId.id,
          state: '0',
          message: data.message,
          receivedId: friendId.id,
        };
        const attributesDouble = {
          userId: friendId.id,
          friendId: userId.id,
          state: '0',
          message: data.message,
        };
        await Repository.getRepository().createFriend(attributes);
        await Repository.getRepository().createFriend(attributesDouble);
        return res.json({ type: 'success' });
      }
        return res.json({ type: 'failed' });
    }
      // eslint-disable-next-line eqeqeq
      if (action[0].state == 1) {
        return res.json({ type: 'exist' });
      }
      // eslint-disable-next-line eqeqeq
      if (action[0].state == 0) {
        return res.json({ type: 'send' });
      }
  }

  async addFriendPhone(req, res) {
    const data = req.body;
    const user = req.session.user.fullName;
    const clauses = { fullName: user };
    const column = ['id'];
    const phoneCheck = { phoneNumber: data.phone };
    const userId = await Repository.getRepository().first(clauses, column);
    const friendId = await Repository.getRepository().first(phoneCheck, column);
    const action = await Repository.getRepository().listBy({ userId: friendId.id, receivedId: req.session.user.id }, ['*']);
    // eslint-disable-next-line eqeqeq
    if (action[0] == undefined) {
      if (friendId) {
        const attributes = {
          userId: userId.id,
          friendId: friendId.id,
          state: '0',
          message: data.message,
          receivedId: friendId.id,
        };
        const attributesDouble = {
          userId: friendId.id,
          friendId: userId.id,
          state: '0',
          message: data.message,
        };
        await Repository.getRepository().createFriend(attributes);
        await Repository.getRepository().createFriend(attributesDouble);

        return res.json({ type: 'success' });
      }
        return res.json({ type: 'failed' });
    }
      // eslint-disable-next-line eqeqeq
      if (action[0].state == 1) {
        return res.json({ type: 'exist' });
      }
      // eslint-disable-next-line eqeqeq
      if (action[0].state == 0) {
        return res.json({ type: 'send' });
      }
  }


  async friendList(req, res) {
    const objectId = await Model.User.find({ userId: req.session.user.id });
    const sortLists = await Model.Conversation.find({ userIds: objectId[0].id }).sort({ msgLatestTime: -1 });
    const { user } = req.session;
    console.log('user ----->',user);
    const friends = await Repository.getRepository().listByJoin({ receivedId: user.id, state: 0 }, ['*']);
    console.log('friends ---->',friends);
    const groupMessages = await Model.Message.find({ conversationId: sortLists[0].id, userId: user.id });
    console.log('groupMessages ---->',groupMessages);
    const listFriends = await Repository.getRepository().listByJoin({ friendId: user.id, state: 1 });
    // eslint-disable-next-line no-console
    return res.render('app/conversation/index', {
      sortLists, user, friends, listFriends, groupMessages, objectId,
    });
  }

  async load(req, res) {
    const objectId = await Model.User.find({ userId: req.session.user.id });
    const sortLists = await Model.Conversation.find({ userIds: objectId[0].id }).sort({ msgLatestTime: -1 });
    const { user } = req.session;
    const friends = await Repository.getRepository().listByJoin({ receivedId: user.id, state: 0 }, ['*']);
    const groupMessages = await Model.Message.find({ conversationId: sortLists[0].id, userId: user.id });
    const listFriends = await Repository.getRepository().listByJoin({ friendId: user.id, state: 1 });
    // eslint-disable-next-line no-console
    return res.render('app/conversation/index', {
      sortLists, user, friends, listFriends, groupMessages,
    });
  }

  async acceptFriend(req, res) {
    const data = req.body;
    await Repository.getRepository().update({ receivedId: req.session.user.id, userId: data.userId }, { state: data.state }, ['*']);
    await Repository.getRepository().update({ receivedId: data.userId, userId: req.session.user.id }, { state: data.state }, ['*']);
    return res.json({ type: 'success' });
  }

  async denyFriend(req, res) {
    const { userId } = req.body;
    await Repository.getRepository().delete({ receivedId: req.session.user.id, userId });
    await Repository.getRepository().delete({ receivedId: userId, userId: req.session.user.id });
    return res.json({ type: 'success' });
  }

  async createGroup(req, res) {
    const id = [];
    const data = req.body;
    const members = JSON.parse(data.groupMember);
    const userId = await Model.User.find({ userId: req.session.user.id });
    id.push(userId[0].id);
    // find id of users
    // eslint-disable-next-line no-plusplus
    if (members.length > 1) {
        // store data into database
        // eslint-disable-next-line no-plusplus
        for (let i = 0; i < members.length; i++) {
          // eslint-disable-next-line no-await-in-loop
          const temp = await Model.User.find({ userId: members[i] });
          id.push(temp[0].id);
          // eslint-disable-next-line eqeqeq
          if (i == members.length - 1) {
            // eslint-disable-next-line no-await-in-loop
            await Model.Conversation.create({
              groupName: data.groupName,
              userIds: id,
              description: data.groupDescrip,
            });
            // eslint-disable-next-line no-await-in-loop
            const date = await Model.Conversation.find({
              groupName: data.groupName,
              userIds: id,
              description: data.groupDescrip,
            });
            // eslint-disable-next-line no-await-in-loop
            await Model.Conversation.update({
              groupName: data.groupName,
              userIds: id,
              description: data.groupDescrip,
            }, {
              $set: { msgLatestTime: date[0].createdAt },
            });
          }
        }
      return res.json({ type: 'success' });
    }
    return res.json({ type: 'fail' });
  }

  async findFriend(req, res) {
    const { string } = req.body;
    // eslint-disable-next-line max-len
    const query = await Model.User.find({ fullName: { $regex: string, $options: 'i' }, userId: { $ne: req.session.user.id } });
    return res.json({ query });
  }

  async loadChat(req, res) {
    const { groupId } = req.body;
    const userId = await Model.User.find({ userId: req.session.user.id });
    const messages = await Model.Message.find({ conversationId: groupId }).populate('memberId');
    return res.json({ messages, userId });
  }

  async messageLoad(req, res) {
    const { groupId } = req.body;
    const userId = await Model.User.find({ userId: req.session.user.id });
    const messages = await Model.Message.find({ conversationId: groupId }).populate('memberId');
    return res.json({ messages, userId });
  }

  async sendMessage(req, res) {
    const dat = req.body;
    const userId = req.session.user.id;
    const find = await Model.User.find({ userId });
    const member = await Model.Conversation.find({ id: dat.groupId });
    console.log('member ---->', member[0].userIds);
    const memberIds = member[0].userIds;
    memberIds.forEach((memberId) => {
      // eslint-disable-next-line eqeqeq
      if (memberId != find[0].id) {
        AuthService.client.to(memberId).emit('newMessage', dat);
      }
    });
    await Model.Message.create({
      memberId: find[0].id,
      content: dat.message,
      conversationId: dat.groupId,
    });
    const temp = await Model.Message.find({
      memberId: find[0].id,
      content: dat.message,
      conversationId: dat.groupId,
    }).sort({ createdAt: -1 });
    await Model.Conversation.updateMany({ id: dat.groupId }, {
      $set: { msgLatestContent: dat.message, msgLatestTime: temp[0].createdAt },
    });
    AuthService.io.to(dat.groupId).emit('message', { message: dat.message, userId });
    return res.json({ type: 'success' });
  }

  async getInfor(req, res) {
    const { userId } = req.body;
    const infor = await Model.User.find({ userId });
    return res.json({ infor });
  }
}


export default AuthService;
