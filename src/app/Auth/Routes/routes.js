import express from 'express';
import multer from 'multer';
// eslint-disable-next-line import/no-cycle
import Controller from '../Controllers/AuthController';
import { verifyNotAuthtication, verifyToken } from '../Middleware/AuthMiddleware';
import { upload } from '../../../utils/s3Upload';

const router = express.Router();
const controller = new Controller();

router.route('/login')
  .get(verifyNotAuthtication, (req, res) => res.render('app/login'))
  .post(controller.loginEmail);

router.route('/login-phone-number')
  .get(verifyNotAuthtication, (req, res) => res.render('app/login-phone-number'))
  .post(controller.loginPhone);

router.route('/register-phone')
  .get((req, res) => res.render('app/auth/register'))
  .post(verifyToken, controller.registerPhone);

router.route('/register-email')
  .get((req, res) => res.render('app/auth/register-email'))
  .post(verifyToken, controller.registerEmail);

router.post('/add-friend-email', controller.addFriendEmail);

router.post('/add-friend-phone', controller.addFriendPhone);

router.put('/accept-friend', controller.acceptFriend);

router.delete('/deny-friend', controller.denyFriend);

router.put('/update-profile', upload.single('avatar'), controller.updateProfile);

router.get('/reset-password', (req, res) => res.render('app/reset-password'));

router.post('/create-group', controller.createGroup);

router.post('/find-friend', controller.findFriend);

router.post('/load-chat', controller.loadChat);

router.post('/send-message', controller.sendMessage);

router.post('/get-user-information', controller.getInfor);

router.post('/messages/:groupId', controller.messageLoad);

router.get('/messages/:groupId', controller.load);

router.get('/logout', controller.logout);


export default router;
