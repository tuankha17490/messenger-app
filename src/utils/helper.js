import sharp from 'sharp';

const resizeImg = picture => sharp(picture.path).resize(262, 317).toFile(`${'public/dist/images'}+${picture.filename}`);
export { resizeImg };
