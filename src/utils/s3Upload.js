import AWS from 'aws-sdk';
import fs from 'fs';
import multer from 'multer';
import path from 'path';

const accessKeyId = process.env.AWS_ACCESS_KEY || 'AKIAZ5QW6SBBCXX6XIKG';
const secretAccessKey = process.env.AWS_SECRET_KEY || '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';

AWS.config.update({
  accessKeyId,
  secretAccessKey,
  region: 'ap-southeast-1',
});

const s3 = new AWS.S3();

const s3Upload = async (file,infor) => {
  const params = {
    Bucket: 'sgroupit-test',
    Key: `khale/${file.width}x${file.height}/${infor.filename.toLowerCase()}`,
    Body: fs.readFileSync(`${infor.path}`),
    ContentType: infor.mimetype,
    ACL: 'public-read',
  };
  const data = await s3.upload(params).promise();

  fs.unlinkSync(`${infor.path}`);
  return data.Location;
};
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public/dist/images');
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
    },
  });
  
const upload = multer({ storage: storage });

export { s3Upload,upload };
