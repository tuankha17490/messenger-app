
exports.up = function(knex) {
    return knex.schema
        .createTable('friends',function(table){
            table.increments('id').primary();
            table.integer('userId').unsigned();
            table.foreign('userId').references('users.id');
            table.integer('friendId').unsigned();
            table.foreign('friendId').references('users.id')
            table.integer('receivedId');
            table.integer('state').notNullable();
            table.text('message');
    })
};

exports.down = (knex) => knex.schema.dropTableIfExists('friends');
