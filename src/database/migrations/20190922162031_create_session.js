
exports.up = function (knex) {
    return knex.schema
        .createTable('user_sessions', (table) => {
            table.varchar('sid').collate('default').primary();
            table.json('sess').notNullable();
            table.timestamp('expire', { precision: 6 }).notNullable();
        });
};

exports.down = function (knex) {
    return knex.schema
        .dropTable('user_sessions');
};
