const path = require('path');
require('dotenv').config();

const BASE_PATH = path.join(__dirname, 'src', 'database');

module.exports = {
  development: {
    client: 'pg',
    connection: { user: 'postgres', password: '628638', database: 'messageapp' },
    migrations: {
      directory: path.join(BASE_PATH, 'migrations'),
    },
    seeds: {
      directory: path.join(BASE_PATH, 'seeds'),
    },
  },
  production: {
    client: 'pg',
    connection: { user: 'postgres', password: '628638', database: 'messageapp' },
    migrations: {
      directory: path.join(BASE_PATH, 'migrations'),
    },
    seeds: {
      directory: path.join(BASE_PATH, 'seeds'),
    },
    pool: {
      min: parseInt(process.env.DB_POOL_MIN, 10),
      max: parseInt(process.env.DB_POOL_MAX, 10),
    },
  },
};
